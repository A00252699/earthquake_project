import java.util.Scanner;

public class EarthquakeStrength {
	private static double in;

	public static double entry(double n) {
		if (n < 4.5) {
			System.out.println("No destruction of buildings. For the value of "+n);
		} else if (n == 4.5 || n < 6.0) {
			System.out.println("Damage to poorly constructed Building. For the value of "+n);
		} else if (n == 6.0 || n < 7.0) {
			System.out.println("Many buildings considerably damaged, some collapse. For the value of "+n);
		} else if (n == 7.0 || n < 8.0) {
			System.out.println("Many buildings destroyed. For the value of "+n);
		} else if (n == 8.0 || n > 8.0) {
			System.out.println("Most structures fall. For the value of "+n);
		}
		return n;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the Richter scale measurement to get the strength of an earthquake: ");
		in = input.nextDouble();
		entry(in);
	}

}
